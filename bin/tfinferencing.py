#!/usr/bin/env python
# coding: utf-8

# # Model laden
import tensorflow.keras.models as models
model = models.load_model("plate.h5")


# # Sliding Window Approach
from PIL import Image, ImageOps
import numpy as np
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt

def contains(area1, area2):
    return area1[0] <= area2[0] and area1[2] > area2[0] and  area1[1] <= area2[1] and area1[3] > area2[1]

xsize = 1800
ysize = 500
step_size = 100
rotation = np.random.randint(3)
img = Image.open("kennzeichen.jpg").rotate(rotation)
img = ImageOps.grayscale(img)
plt.imshow(img, cmap="gray_r")
print("Ratation: ", rotation)
plate_areas = []
for y in range(0, img.size[1] - ysize, step_size):
    for x in range(0, img.size[0] - xsize, step_size):
        #print(x, y, x + xsize, y + ysize)
        part = img.crop((x, y, x + xsize, y + ysize))
        data = np.asarray(part.resize((44,18), resample=Image.BICUBIC))
        data = data.astype(np.float32)/255
        pred = model.predict(data.reshape(-1, 18, 44, 1)) # Anzahl der Beispiele = 1, Höhe = 18, Breite = 44, Farbkanäle = 1
        if pred > 0.6:
            new_area = [x, y, x + xsize, y + ysize]
            if len(plate_areas) > 0 and contains(plate_areas[-1], new_area):
                plate_areas[-1] = [np.min([plate_areas[-1][0], x]), np.min([plate_areas[-1][1], y]), np.max([plate_areas[-1][2], xsize]), np.max([plate_areas[-1][3], ysize])]
                part = img.crop((plate_areas[-1][0], plate_areas[-1][1], plate_areas[-1][0] + plate_areas[-1][2], plate_areas[-1][1] + plate_areas[-1][3]))
                data = np.asarray(part.resize((44,18), resample=Image.BICUBIC))
            else:
                plate_areas.append(new_area)
            plt.imshow(data, cmap="gray_r")
            plt.show()
            print(pred)