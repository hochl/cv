#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from pathlib import Path
import os, sys
import random
from PIL import Image, ImageOps
from shutil import copyfile
import json
import os
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Flatten, Dropout
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from minio import Minio
from minio.error import S3Error
import matplotlib.pyplot as plt

client = Minio(
    os.environ["ENDPOINT_URL"],
    access_key = os.environ["AWS_ACCESS_KEY"],
    secret_key = os.environ["AWS_SECRET_KEY"]
)
bucketname = os.environ["BUCKET_NAME"]

def open_image(filename):
    img = Image.open(filename)
    #print(len(img.getdata()))
    img = ImageOps.grayscale(img)
    #print(list(img.getdata()))
    return np.array(img.getdata()).astype(np.float32)
    
def open_labels(filename):
    with open(filename) as json_file:
        data = json.load(json_file)
    return data

client.fget_object(bucketname, "plate/label.json", "label.json")
data = open_labels("label.json")

# Zusätzliche Daten generieren lassen
#gen = ImageDataGenerator(width_shift_range=2, height_shift_range=2)
x_train = []
y_train = data.values()

for img_name in data:
    client.fget_object(bucketname, "plate/train/" + img_name, "image.png")
    pixels = open_image('image.png')
    if len(x_train) == 0:
        x_train = [pixels]
    else:
        x_train = np.append(x_train, [pixels], axis=0)

x_train = np.array(x_train)
y_train = map(int, y_train)
y_train = np.fromiter(y_train, dtype=np.int)

#for batch in gen.flow(x_train, y_train):
#    print(batch[0])

x_train, x_test, y_train, y_test = train_test_split(x_train, y_train)
img_count = len(x_train)
x_train = x_train.astype(np.float32) / 255
x_test = x_test.astype(np.float32) / 255


# Model mit allen Layern bauen
image_size = len(x_train[0])

model = Sequential()

model.add(Conv2D(300, kernel_size=(5, 5), activation="relu", input_shape=(18, 44, 1), padding="same"))
model.add(MaxPooling2D(pool_size = (2,2)))
model.add(Dropout(0.25))
model.add(Conv2D(300, kernel_size=(5, 5), activation="relu", padding="same"))
model.add(MaxPooling2D(pool_size = (2,2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(1, activation="sigmoid"))

model.compile(optimizer="adam", loss="binary_crossentropy", metrics="accuracy")

from tensorflow.keras.callbacks import ModelCheckpoint
save_model = ModelCheckpoint("weights.{epoch:02d}-{loss:.2f}.hdf5") #Die Variable muss dann der fit-Methode im Parameter callbacks übergeben werden.

x_train.reshape(img_count, image_size)

model.fit(
    x_train.reshape(img_count, 18, 44, 1),
    y_train,
    epochs=10,
    batch_size=1000, callbacks=[save_model])

print(y_test[1])

model.save("plate.h5") #Model speichern für eine spätere Verwendung
client.fput_object(bucketname, "plate/plate.h5", "plate.h5")
#plt.imshow(x_test[1].reshape(18,44), cmap="gray_r")
#plt.show()
#model.summary()

#model.predict(x_test[1].reshape(1, 18, 44, 1)) # anzahl der Beispiele = 1, Höhe = 18, Breite = 44, Farbkanäle = 1 

# # Gewichte visualisieren
#filter_data = K.eval(model.layers[0].weights[0])
#print(filter_data)
#plt.imshow(filter_data[:, :, :, 200].reshape(5,5))


# # GPU Testen
#print(tf.test.gpu_device_name())

